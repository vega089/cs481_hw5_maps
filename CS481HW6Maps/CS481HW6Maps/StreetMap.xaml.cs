﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CS481HW6Maps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StreetMap : ContentPage
    {
        private int mode;

        public StreetMap()
        {
            InitializeComponent();
            Map MapStreet = new Map
            {
                MapType = MapType.Street

            };
            MapSpan.FromCenterAndRadius(new Position(99, -122), Distance.FromMiles(10));
            // Position mapPosition = new Position(41.9028, 12.4964);

            // var position1 = new Position(41.9028, 12.4964);
            var position1 = new Position(34.095556, -118.126715);
            var position4 = new Position(34.033976, -118.097720);
            var position2 = new Position(34.036360, -118.097464);
            var position3 = new Position(34.009791, -118.49601);



            Pin STmapPin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Grill em all",
                Address = "Alhambra,CA",

            };
            MapStreet.Pins.Add(STmapPin1);

            Pin STmapPin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "In N Out Burger",
                Address = "5500 Market Pl Dr, Monterey Park, CA 91755",

            };
            MapStreet.Pins.Add(STmapPin2);

            Pin STmapPin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "Santa Monica Pier",
                Address = "200 Santa Monica Pier, Santa Monica, CA 90401",

            };
            MapStreet.Pins.Add(STmapPin3);

            Pin STmapPin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "AMC movie Theatre",
                Address = "1475 N Montebello Blvd, Montebello, CA 90640",

            };
            MapStreet.Pins.Add(STmapPin4);


            MainPickerST.Items.Add("GrillEmAll");
            MainPickerST.Items.Add("InNOut");
            MainPickerST.Items.Add("SantaMonicaPier");
            MainPickerST.Items.Add("AMCMovies");
            mode = MainPickerST.SelectedIndex;
        }

        private void MainPickerST_SelectedIndexChanged(object sender, EventArgs e)
        {
            var name = MainPickerST.Items[MainPickerST.SelectedIndex];
            // var position1 = new Position(41.9028, 12.4964);
            var position1 = new Position(34.095556, -118.126715);
            var position4 = new Position(34.033976, -118.097720);
            var position2 = new Position(34.036360, -118.097464);
            var position3 = new Position(34.009791, -118.49601);


            if (name == "GrillEmAll")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position1, Distance.FromKilometers(0.444));
                MapStreet.MoveToRegion(mapSpan);
            }
            else if (name == "InNOut")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position2, Distance.FromKilometers(0.444));
                MapStreet.MoveToRegion(mapSpan);
            }
            else if (name == "SantaMonicaPier")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position3, Distance.FromKilometers(0.444));
                MapStreet.MoveToRegion(mapSpan);
            }
            else if (name == "AMCMovies")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position4, Distance.FromKilometers(0.444));
                MapStreet.MoveToRegion(mapSpan);
            }




        }
    }
}