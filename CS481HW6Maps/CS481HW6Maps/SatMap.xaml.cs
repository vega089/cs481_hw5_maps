﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CS481HW6Maps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SatMap : ContentPage
    {
        private int mode;

        public SatMap()
        {
            InitializeComponent();
            Map Smap = new Map
            {
                MapType = MapType.Satellite
              
            };
            MapSpan.FromCenterAndRadius(new Position(99, -122), Distance.FromMiles(10));
            // Position mapPosition = new Position(41.9028, 12.4964);

            // var position1 = new Position(41.9028, 12.4964);
            var position1 = new Position(34.095556, -118.126715);
            var position4 = new Position(34.033976, -118.097720);
            var position2 = new Position(34.036360, -118.097464);
            var position3 = new Position(34.009791, -118.49601);



            Pin mapPin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Grill em all",
                Address = "Alhambra,CA",

            };
            SMap.Pins.Add(mapPin1);

            Pin mapPin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "In N Out Burger",
                Address = "5500 Market Pl Dr, Monterey Park, CA 91755",

            };
            SMap.Pins.Add(mapPin2);

            Pin mapPin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "Santa Monica Pier",
                Address = "200 Santa Monica Pier, Santa Monica, CA 90401",

            };
            SMap.Pins.Add(mapPin3);

            Pin mapPin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "AMC movie Theatre",
                Address = "1475 N Montebello Blvd, Montebello, CA 90640",

            };
            SMap.Pins.Add(mapPin4);


            MainPicker.Items.Add("GrillEmAll");
            MainPicker.Items.Add("InNOut");
            MainPicker.Items.Add("SantaMonicaPier");
            MainPicker.Items.Add("AMCMovies");
            mode = MainPicker.SelectedIndex;
        }

        private void MainPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var name = MainPicker.Items[MainPicker.SelectedIndex];
            // var position1 = new Position(41.9028, 12.4964);
            var position1 = new Position(34.095556, -118.126715);
            var position4 = new Position(34.033976, -118.097720);
            var position2 = new Position(34.036360, -118.097464);
            var position3 = new Position(34.009791, -118.49601);

            
            if (name == "GrillEmAll")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position1, Distance.FromKilometers(0.444));
                SMap.MoveToRegion(mapSpan);
            }
          else  if (name == "InNOut")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position2, Distance.FromKilometers(0.444));
                SMap.MoveToRegion(mapSpan);
            }
            else if (name == "SantaMonicaPier")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position3, Distance.FromKilometers(0.444));
                SMap.MoveToRegion(mapSpan);
            }
            else if (name == "AMCMovies")
            {
                MapSpan mapSpan = MapSpan.FromCenterAndRadius(position4, Distance.FromKilometers(0.444));
                SMap.MoveToRegion(mapSpan);
            }




        }
    }
}